package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[5];
        System.out.println(Arrays.toString(array));

        array = ArrayMethods.add(2, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(3, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(4, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(5, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(2, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(4, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(6, array);
        System.out.println(Arrays.toString(array));
        array = ArrayMethods.add(2, array);
        System.out.println(Arrays.toString(array));  
        array = ArrayMethods.add(1, array);
        System.out.println(Arrays.toString(array));

        array = ArrayMethods.delete(2, array);
        System.out.println(Arrays.toString(array));



        EpamArray arr = new EpamArray(5);
        arr.add(1);
        arr.add(2);
        arr.add(1);
        arr.add(2);
        arr.add(1);
        arr.add(2);
        arr.print();
        arr.add(1);
        arr.add(2);
        arr.print();
        arr.deleteValue(2);
        arr.print();
    }
}


