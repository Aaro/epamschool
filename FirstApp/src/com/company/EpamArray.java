package com.company;

import java.util.Arrays;

class EpamArray {
    private int count;
    private int[] array;

    EpamArray(int capacity) {
        count = 0;
        array = new int[capacity];
    }

 /*   int get(int index) {
        if (index > count - 1 || index < 0) {
            return -1;
        }
        return array[index];
    }

    void set(int val, int index) {
        if (index > count - 1 || index < 0) {
            return;
        }
        array[index] = val;
    }
  */

    void add(int val) {
        int len = array.length;
        if (count == len) {
            int[] newArray = new int[len * 2];
            for (int i = 0; i < len; i++) {
                newArray[i] = array[i];
            }
            newArray[len] = val;
            array = newArray;
            count++;
        } else if (count < len) {
            array[count] = val;
            count++;
        }
    }

    void deleteValue(int val) {
        int len = array.length;
        for (int i = 0; i < len; i++) {
            if (array[i] == val) {
                array[i] = 0;
                this.moveElements(i);
                count--;
            }
        }
    }

    private void moveElements(int index) {
        int len = array.length;
        int tmp = 0;
        for (int i = index; i < len - 1; i++) {
            tmp = array[i];
            array[i] = array[i + 1];
            array[i + 1] = tmp;
        }
    }

    void print() {
        System.out.println(Arrays.toString(array));
    }
}
