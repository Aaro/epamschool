package com.company;

class ArrayMethods {

    static int[] add(int m, int[] array) {
        int len = array.length;
        for (int i = 0; i < len; i++) {
            if (array[i] == 0) {
                array[i] = m;
                if (i != len -1) {
                    return  array;
                }
                if (i == len - 1) {
                    int[] newArray = new int[len * 2];
                    for (int j = 0; j < len; j++) {
                        newArray[j] = array[j];
                    }
                    return newArray;
                }
            }
        }
        return array;
    }

    static int[] delete(int m, int[] array) {
        int len = array.length;
        if (len != 0) {
            int count = 0;
            for (int anArray : array) {
                if (anArray == m) {
                    count++;
                }
            }
            if (count > 0) {
                int[] newArray = new int[len - count];
                int index = 0;
                for (int anArray : array) {
                    if (anArray != m) {
                        newArray[index] = anArray;
                        index++;
                    }
                }
                return newArray;
            }
        }
        return array;
    }
}
