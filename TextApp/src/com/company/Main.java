package com.company;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        File folder = new File(System.getProperty("user.dir"));
        File[] listOfFiles = folder.listFiles();

        ArrayList<String> listOfPathTxtFiles = new ArrayList<>();

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile() && file.getName().endsWith(".txt")) {
                    listOfPathTxtFiles.add(file.getPath());
                }
            }
        }

        if (listOfPathTxtFiles.size() >= 2) {
            List list1 = TxtFileDownloader.readFileInList(listOfPathTxtFiles.get(0));
            List list2 = TxtFileDownloader.readFileInList(listOfPathTxtFiles.get(1));
            List<String[]> answer = TextManager.compareLists(list1, list2);
            CSVManager.writeInCsvFile(answer);
        }
    }
}

