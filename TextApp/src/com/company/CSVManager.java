package com.company;


import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


class CSVManager {
    static void writeInCsvFile(List<String[]> list) throws IOException {
        FileWriter writer = new FileWriter("/Users/nadezdasalnova/EpamSchool/TextApp/sto1.csv");
        String nameString = "№, file1, file2\n";
        writer.write(nameString);
        for (String[] line : list) {
            StringBuilder sb = new StringBuilder();
            for (String s : line) {
                sb.append(s).append(",");
            }
            writer.write(sb.toString().substring(0,sb.toString().length()-1) + "\n");
        }
        writer.close();
    }
}
