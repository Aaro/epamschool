package com.company;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

class  TextManager {

    static List<String[]> compareLists(List<String> list1, List<String> list2) {

        List<String[]> answer = new ArrayList<>();

        int count1 = list1.size();
        int count2 = list2.size();
        int index = 0;

        String str1;
        String str2;

        Iterator<String> itr1 = list1.iterator();
        Iterator<String> itr2 = list2.iterator();

        if (count1 <= count2) {
            while (itr1.hasNext()) {
               str1 = itr1.next();
               str2 = itr2.next();
               if (!str1.equals(str2)) {
                   answer.add(new String[] {String.valueOf(index) ,"0", str2});
               }
               index++;
            }
            while (itr2.hasNext()) {
                str2 = itr2.next();
                answer.add(new String[] {String.valueOf(index) ,"-", str2});
                index++;
            }
        } else {
            while (itr2.hasNext()) {
                str1 = itr1.next();
                str2 = itr2.next();
                if (!str1.equals(str2)) {
                    answer.add(new String[] {String.valueOf(index) ,"0", str2});
                }
                index++;
            }
            while (itr1.hasNext()) {
                str1 = itr1.next();
                answer.add(new String[] {String.valueOf(index) ,str1, "-"});
                index++;
            }
        }
        return  answer;
    }
}
